class PostIdsController < ApplicationController
  before_action :set_post_id, only: [:show, :edit, :update, :destroy]

  # GET /post_ids
  # GET /post_ids.json
  def index
    @post_ids = PostId.all
  end

  # GET /post_ids/1
  # GET /post_ids/1.json
  def show
  end

  # GET /post_ids/new
  def new
    @post_id = PostId.new
  end

  # GET /post_ids/1/edit
  def edit
  end

  # POST /post_ids
  # POST /post_ids.json
  def create
    @post_id = PostId.new(post_id_params)

    respond_to do |format|
      if @post_id.save
        format.html { redirect_to @post_id, notice: 'Post was successfully created.' }
        format.json { render :show, status: :created, location: @post_id }
      else
        format.html { render :new }
        format.json { render json: @post_id.errors, status: :unprocessable_entity }
      end
    end
  end

  # PATCH/PUT /post_ids/1
  # PATCH/PUT /post_ids/1.json
  def update
    respond_to do |format|
      if @post_id.update(post_id_params)
        format.html { redirect_to @post_id, notice: 'Post was successfully updated.' }
        format.json { render :show, status: :ok, location: @post_id }
      else
        format.html { render :edit }
        format.json { render json: @post_id.errors, status: :unprocessable_entity }
      end
    end
  end

  # DELETE /post_ids/1
  # DELETE /post_ids/1.json
  def destroy
    @post_id.destroy
    respond_to do |format|
      format.html { redirect_to post_ids_url, notice: 'Post was successfully destroyed.' }
      format.json { head :no_content }
    end
  end

  private
    # Use callbacks to share common setup or constraints between actions.
    def set_post_id
      @post_id = PostId.find(params[:id])
    end

    # Never trust parameters from the scary internet, only allow the white list through.
    def post_id_params
      params.require(:post_id).permit(:interger, :body)
    end
end
